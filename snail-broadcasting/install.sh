#!/bin/bash

# This runs as root on the server
# ./install.sh chef_json_file.json

set -e

chef_binary=$(which chef-solo 2>/dev/null)

# Are we on a vanilla system?
if ! test -f "$chef_binary"; then
  echo "You have to install Chef!"
  exit 1
fi

chef-solo -c solo.rb -j $1
