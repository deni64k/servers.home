#!/bin/sh

# Usage: ./deploy.sh
#
# It infers the target for the deploy by the files under deploy_target_dir (see below).
# For each host you want to deploy to, put a file in there. Eg, for deploying to "alpha", put alpha.json in.
#
# ls ./deploy_targets
# => alpha.json
# ./deploy.sh
# => (deploys to alpha using alpha.json)

set -e

deploy_target_dir="./deploy_targets"

berks install --path vendor/cookbooks

for path_to_json in $deploy_target_dir/*.json; do
  file=$(basename "$path_to_json")
  host=negval@${file%.json}

  # The host key might change when we instantiate a new VM, so
  # we remove (-R) the old host key from known_hosts
  # ssh-keygen -R "${host#*@}" 2> /dev/null

  # tar cj . | ssh -o 'StrictHostKeyChecking no' "$host" '
  # sudo rm -rf ~/chef &&
  # mkdir ~/chef &&
  # cd ~/chef &&
  # tar xj &&
  # sudo bash install.sh '$path_to_json

  rsync=$(ssh "${host}" 'which rsync 2>/dev/null')
  if [ "${rsync}" != "" ]; then
    rsync --cvs-exclude --exclude=revision-deploys --exclude=preseed --exclude=src --delete -r . -e ssh $host:chef
  else
    ssh "${host}" '[ -d ~/chef ] && sudo rm -rf ~/chef && mkdir ~/chef'
    scp -qrC . "${host}":chef/
  fi
  ssh "${host}" 'cd ~/chef && sudo bash install.sh '${path_to_json}
done
