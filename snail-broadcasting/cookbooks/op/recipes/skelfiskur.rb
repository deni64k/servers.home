package 'pkg-config'
package 'autoconf'
package 'automake'
package 'yasm'
package 'libtool'
package 'libgpac-dev'
package 'zlib1g-dev'

package 'libvpx-dev'
package 'libopus-dev'
package 'libmp3lame-dev'
package 'libvorbis-dev'
package 'libass-dev'
package 'libtheora-dev'
package 'libopenjpeg-dev'
package 'libv4l-dev'
package 'libssl-dev'

git "#{Chef::Config[:file_cache_path]}/src/libvpx" do
  repository 'http://git.chromium.org/webm/libvpx.git'
  revision 'master'
  notifies :run, 'bash[install_libvpx]'
  notifies :run, 'bash[install_ffmpeg]'
end

git "#{Chef::Config[:file_cache_path]}/src/fdk-aac" do
  repository 'git://github.com/mstorsjo/fdk-aac.git'
  revision 'master'
  notifies :run, 'bash[install_fdk-aac]'
  notifies :run, 'bash[install_ffmpeg]'
end

git "#{Chef::Config[:file_cache_path]}/src/x264" do
  repository 'git://git.videolan.org/x264.git'
  revision 'master'
  notifies :run, 'bash[install_x264]'
  notifies :run, 'bash[install_ffmpeg]'
end

git "#{Chef::Config[:file_cache_path]}/src/ffmpeg" do
  repository 'git://source.ffmpeg.org/ffmpeg.git'
  revision 'release/2.1'
  notifies :run, 'bash[install_ffmpeg]'
end

bash 'install_libvpx' do
  action :nothing
  cwd "#{Chef::Config[:file_cache_path]}/src/libvpx"
  code <<EOF
autoreconf -fiv
./configure --prefix=/usr/local/apps/libvpx --disable-examples
make
make install
make clean
EOF
end

bash 'install_fdk-aac' do
  action :nothing
  cwd "#{Chef::Config[:file_cache_path]}/src/fdk-aac"
  code <<EOF
autoreconf -fiv
./configure --prefix=/usr/local/apps/fdk-aac --disable-shared
make
make install
make distclean
EOF
end

bash 'install_x264' do
  action :nothing
  cwd "#{Chef::Config[:file_cache_path]}/src/x264"
  code <<EOF
./configure --prefix=/usr/local/apps/x264 --enable-static
make
make install
make distclean
EOF
end

bash 'install_ffmpeg' do
  action :nothing
  cwd "#{Chef::Config[:file_cache_path]}/src/ffmpeg"
  code <<EOF
./configure --prefix=/usr/local/apps/ffmpeg                                                                             \
            --disable-ffplay --disable-ffprobe                                                                          \
            --disable-doc                                                                                               \
            --extra-libs="-ldl"                                                                                         \
            --extra-cflags="-I/usr/local/apps/x264/include -I/usr/local/apps/fdk-aac/include -I/usr/local/apps/libvpx/include" \
            --extra-ldflags="-L/usr/local/apps/x264/lib -L/usr/local/apps/fdk-aac/lib -L/usr/local/apps/libvpx/lib"     \
            --disable-runtime-cpudetect --enable-libass --enable-libfdk-aac --enable-libopenjpeg                        \
            --enable-libtheora --enable-libv4l2 --enable-libvorbis --enable-libvpx --enable-libx264 --enable-libmp3lame \
            --enable-openssl --enable-hardcoded-tables --enable-gpl --enable-version3 --enable-nonfree
make
make install
EOF
  environment 'PKG_CONFIG_PATH' => '/usr/local/apps/x264/lib/pkgconfig'
end

package 'subversion'

directory '/usr/local/apps/crtmpserver'
subversion 'crtmpserver' do
  # repository 'https://svn.rtmpd.com/crtmpserver/branches/1.0'
  repository 'https://svn.rtmpd.com/crtmpserver/trunk'
  revision 'HEAD'
  destination '/usr/local/apps/crtmpserver/trunk'
  action :sync
  svn_username 'anonymous'
  svn_password '""'

  notifies :run, 'bash[install_crtmpserver]', :immediately
  notifies :create, 'template[/usr/local/apps/crtmpserver/current/crtmpserver.lua]', :immediately
  notifies :restart, 'runit_service[crtmpserver]', :delayed
end

template '/usr/local/apps/crtmpserver/current/crtmpserver.lua' do
  source 'crtmpserver.lua.erb'
  mode 0440
  group 'negval'
  notifies :restart, 'runit_service[crtmpserver]', :delayed
end

bash 'install_crtmpserver' do
  # action :nothing
  cwd '/usr/local/apps/crtmpserver/trunk/builders/cmake'
  code <<EOF
COMPILE_STATIC=1 cmake -DCMAKE_BUILD_TYPE=Release .
if [ "$?" -ne "0" ]; then
  echo "cmake failed";
  exit 1;
fi

make
if [ "$?" -ne "0" ]; then
  echo "build failed";
  exit 1;
fi

ln -sf /usr/local/apps/crtmpserver/trunk /usr/local/apps/crtmpserver/current
EOF
end

deploy_revision '/usr/local/apps/snail-bedroom' do
  repository 'git@bitbucket.org:negval/snail-bedroom.git'
  user 'negval'
  group 'www-data'
  shallow_clone true
  keep_releases 3

  migrate false
  purge_before_symlink ['log']
  create_dirs_before_symlink ['log']
  symlink_before_migrate.clear
  symlinks 'log' => 'log'

  notifies :restart, 'runit_service[crtmpserver]', :delayed
  notifies :restart, 'runit_service[blowout]', :delayed
  notifies :restart, 'runit_service[blowout-webm]', :delayed
end

deploy_revision '/usr/local/apps/snail-site' do
  repository 'git@bitbucket.org:negval/snail-site.git'
  user 'negval'
  group 'www-data'
  shallow_clone true
  keep_releases 3

  migrate false
  purge_before_symlink.clear # ['log', 'tmp/pids', 'public/system']
  create_dirs_before_symlink.clear # ['tmp', 'public', 'config']
  symlink_before_migrate.clear # {'config/database.yml'=>'config/database.yml'}
  symlinks.clear # {'system'=>'public/system', 'pids'=>'tmp/pids', 'log'=>'log'}

  before_symlink do
    link "#{release_path}/hls" do
      to '/run/shm/snail/hls'
    end
  end

  notifies :reload, 'service[nginx]', :delayed
end

runit_service 'crtmpserver' do
  log false
end

runit_service 'blowout' do
  log false
end

runit_service 'blowout-webm' do
  log false
end

directory '/run/shm/snail/hls' do
  user 'negval'
  group node['nginx']['group']
  recursive true
end

directory '/var/log/nginx/snail' do
  owner node['nginx']['user']
end

template '/etc/nginx/sites-available/snail' do
  source 'nginx-snail.erb'
  notifies :create, 'link[/etc/nginx/sites-enabled/snail]', :immediately
  notifies :delete, 'link[/etc/nginx/sites-enabled/000-default]', :immediately
  notifies :reload, 'service[nginx]', :delayed
end

link '/etc/nginx/sites-enabled/snail' do
  to '/etc/nginx/sites-available/snail'
  not_if 'test -L /etc/nginx/sites-enabled/snail'
  notifies :restart, 'service[nginx]', :delayed
end
link '/etc/nginx/sites-enabled/000-default' do
  action :delete
  only_if 'test -L /etc/nginx/sites-enabled/000-default'
  notifies :restart, 'service[nginx]', :delayed
end
