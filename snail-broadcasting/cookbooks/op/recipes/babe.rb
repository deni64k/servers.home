package 'ffmpeg'

runit_service 'sucking' do
  log false
end

deploy_revision '/usr/local/apps/snail-sucker' do
  repo 'git@bitbucket.org:negval/snail-sucker.git'
  user 'negval'
  group 'www-data'
  shallow_clone true
  keep_releases 3

  migrate false
  purge_before_symlink ['log']
  create_dirs_before_symlink ['log']
  symlink_before_migrate.clear
  symlinks 'log' => 'log'

  notifies :restart, 'runit_service[sucking]', :delayed
end