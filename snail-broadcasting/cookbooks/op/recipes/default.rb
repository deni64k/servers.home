package 'ntp'
package 'htop'
package 'sysstat'
package 'strace'
package 'ltrace'
package 'lsof'

package 'rsync'
package 'curl'
package 'wget'

package 'git'

package 'mg'

file '/etc/locale.gen' do
  content "en_US.UTF-8 UTF-8\nru_RU.UTF-8 UTF-8\n"
  notifies :run, 'execute[run-locale-gen]', :immediately
end
execute 'run-locale-gen' do
  command 'locale-gen'
  action :run
end

ssh_known_hosts_entry 'bitbucket.org'

directory '/usr/local/apps'
