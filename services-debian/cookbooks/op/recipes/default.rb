package 'ntp'
package 'htop'
package 'sysstat'
package 'strace'
package 'ltrace'

package 'rsync'

package 'git'
package 'subversion'

package 'build-essential'
package 'libboost-all-dev'

package 'mg'

file '/etc/locale.gen' do
  content "en_US.UTF-8 UTF-8\nru_RU.UTF-8 UTF-8\n"
end
execute 'run-locale-gen' do
  command 'locale-gen'
  action :run
end

sysctl_param 'kernel.shmmax' do
  value 3 * (1024**3)
end
sysctl_param 'kernel.shmall' do
  value 4 * (1024**3) / 4096
end

{'listen_addresses'             => '*',
 'shared_buffers'               => '2GB',
 'work_mem'                     => '2MB',
 'maintenance_work_mem'         => '128MB',
 'effective_cache_size'         => '2GB',
 'checkpoint_completion_target' => '0.7',
 'lc_messages'                  => 'en_US.UTF-8',
 'default_text_search_config'   => 'pg_catalog.english',
 'ssl'                          => false
}.each do |k, v|
  node.default['postgresql']['config'][k] = v
end
node.default['postgresql']['pg_hba'] = [
    {type: 'local', db: 'all', user: 'postgres', addr: nil,              method: 'peer'},
    {type: 'local', db: 'all', user: 'all',      addr: nil,              method: 'peer'},
    {type: 'host',  db: 'all', user: 'all',      addr: '127.0.0.1/32',   method: 'md5'},
    {type: 'host',  db: 'all', user: 'all',      addr: '::1/128',        method: 'md5'},
    {type: 'host',  db: 'all', user: 'all',      addr: '192.168.0.1/24', method: 'md5'}
  ]

bash 'create-role-and-database-rocketjump' do
  user 'postgres'
  code <<-EOH
echo "CREATE ROLE rocketjump LOGIN PASSWORD 'rocketjump';" | psql
echo "CREATE DATABASE rocketjump OWNER rocketjump;" | psql
  EOH
  action :run
end

bash 'create-role-and-database-negval' do
  user 'postgres'
  code <<-EOH
echo "CREATE ROLE negval LOGIN PASSWORD 'negval';" | psql
echo "CREATE DATABASE negval OWNER negval;" | psql
  EOH
  action :run
end

template "#{node['djbdns']['tinydns_dir']}/root/data" do
  source 'tinydns-data.erb'
  mode 00644
  notifies :run, 'execute[build-tinydns-data]'
end
